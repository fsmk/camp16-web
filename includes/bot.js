(function($) {
    var data = {};

    var form = [
        {
            key: 'name',
            prompt: 'Hey there! Tell me your name?',
            validate: function (val) {
                return true;
            },
            helpText: 'Full name',
        },
        {
            key: 'college',
            prompt: function () {
                return 'Howdy ' + data.name + '! So which college do you go to?';
            },
            validate: function (val) {
                return true;
            },
        },
        {
            key: 'semester',
            prompt: 'And semester?',
            options: [
                '2',
                '4',
                '6',
                '8',
                'N/A',
            ],
        },
        {
            key: 'gender',
            prompt: 'Nice. So which gender do you identify as?',
            options: [
                'male',
                'female',
                'other',
            ],
        },
        {
            key: 'phone',
            prompt: 'Awesome! How can I call ya?',
            validate: function (val) {
                return true;
            },
            helpText: 'Phone number',
        },
        {
            key: 'email',
            prompt: 'And email?',
            helpText: 'E-mail address',
        },
        {
            key: 'city',
            prompt: 'Great going. So which city did you say you were from?',
            helpText: 'City of residence',
        },
        {
            key: 'firstCamp',
            prompt: 'Cool. So is this going to be your first FSMK camp?',
            options: [
                'Yes, this is my first',
                'No, I\'ve attended an FSMK camp before',
            ],
        },
        {
            key: 'track',
            prompt: 'Aah, nice! So which track will you be choosing?',
            options: [
		            //'Web Front-End',
		            //'Web Back-End',
		            'Design + Animation',
                'Freedom Hardware',
            ],
        },
        {
            key: 'laptop',
            prompt: 'Good choice. You\'ll be getting a laptop right?',
            options: [
                'Yes',
                'No',
                'Maybe',
            ],
        },
        {
            key: 'tshirt',
            prompt: 'Wokay! So what would be your tee-shirt size?',
            options: [
                'S',
                'M',
                'L',
                'XL',
                'XXL',
            ],
        },
    ];

    var curField = 0;

    function setPrompt(prompt) {
        $('.bot .message').text(prompt);
    }

    function getPrompt(field) {
        if (typeof field.prompt === 'function') {
            return field.prompt();
        }

        return field.prompt;
    }

    function setCurPrompt() {
        var field = form[curField];
        var prompt = getPrompt(field);

        if (field.helpText) {
            prompt += ' (' + field.helpText + ')';
        }

        $('.options').html('');

        if ('options' in field) {
            for (var j in field.options) {
                var option = field.options[j];
                var $option = $($.parseHTML('<span></span>')).text(option).data('val', option);

                $('.options').append($option);
            }
        }

        setPrompt(prompt);
    }

    function processInput(val) {
        if (curField === Infinity) {
            $('.options').html('');
            $('.textIn .textInput').val('');

            if (val.toLowerCase() === 'yes') {
                $.post('/action/register_bot.php', data, function () {
                    setPrompt('Done! We\'ll get back to you soon.');
                });

                curField = -1;
            }
        }

        if (curField === -1) {
            return;
        }

        var field = form[curField];

        if ('validate' in field) {
            var valid = field.validate(val);

            if (valid !== true) {
                setPrompt('Whoops! ' + valid + '\n' + getPrompt(field));
                return;
            }
        }

        if ('options' in field) {
            if (!~field.options.indexOf(val.toLowerCase()) && !~field.options.indexOf(val)) {
                setPrompt('Whoops! Invalid option\n' + getPrompt(field));
                return;
            }
        }

        data[field.key] = val;

        if (curField >= form.length - 1) {
            curField = Infinity;

            $('.options').html('');

            var options = ['Yes', 'No'];

            for (var j in options) {
                var option = options[j];
                var $option = $($.parseHTML('<span></span>')).text(option).data('val', option);

                $('.options').append($option);
            }

            setPrompt('Whew! All done. Ready to submit?');
        } else {
            curField++;
            setCurPrompt();
        }

        $('.textIn .textInput').val('');
    }

    $(function () {
        $('.inputForm').on('submit', function (e) {
            e.preventDefault();
            processInput($('.textIn .textInput').val());
        });

        $('.options').on('click', 'span', function (e) {
            processInput($(this).data('val'));
        });
    });

    setCurPrompt();

    window.printData = function () {
        console.log(data);
    };
}(jQuery));

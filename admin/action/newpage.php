<?php
session_start();
require '../includes/config.php';
if ($_SESSION['signin_check'] == 1) {
  $title = $_POST['title'];
  $title = $conn->real_escape_string($title);
  $content = $_POST['content'];
  $content = $conn->real_escape_string($content);
  $slug = $_POST['slug'];
  $slug = $conn->real_escape_string($slug);
  $nav = $_POST['nav'];
  $nav = $conn->real_escape_string($nav);
  $sql = "INSERT INTO pages (Title, Content, Slug, Nav) values ('$title', '$content', '$slug', '$nav')";
  if (mysqli_query($conn, $sql)) {
    header("Location: $baseurl/view/allpages.php");
  }
  else {
    echo "Error - contingency activated - contact admin";
  }
}
else {
  header("Location: $baseurl/view/signin.php");
}
?>

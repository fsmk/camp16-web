<?php
session_start();
require '../includes/config.php';
if ($_SESSION['signin_check'] == 1) {
  $title = $_POST['title'];
  $title = $conn->real_escape_string($title);
  $content = $_POST['content'];
  $content = $conn->real_escape_string($content);
  $slug = $_POST['slug'];
  $slug = $conn->real_escape_string($slug);
  $date = date('Y-m-d H:i:s');
  $sql = "INSERT INTO posts (Title, Content, Slug, Date) values ('$title', '$content', '$slug', '$date')";
  if (mysqli_query($conn, $sql)) {
    header("Location: $baseurl/view/allposts.php");
  }
  else {
    echo "Error - contingency activated - contact admin";
  }
}
else {
  header("Location: $baseurl/view/signin.php");
}
?>

<?php
  session_start();
  require '../includes/config.php';
  require '../includes/head.php';
  if ($_SESSION['signin_check'] == 1) {
    $id = $_GET['id'];
    $sql = "SELECT * FROM participants WHERE ID='$id'";
    $participant = mysqli_query($conn, $sql);
    $participant = mysqli_fetch_object($participant);
?>

<body>
  <?php
    require '../includes/nav.php';
  ?>
  <div class="container">

    <h2>View Participant</h2>

    <table class="table">
      <tbody>
        <tr>
          <td>ID</td>
          <td><?=$participant->ID ?></td>
        </tr>
        <tr>
          <td>Name</td>
          <td><?=$participant->Name ?></td>
        </tr>
        <tr>
          <td>Email</td>
          <td><?=$participant->Email ?></td>
        </tr>
        <tr>
          <td>Phone</td>
          <td><?=$participant->Phone ?></td>
        </tr>
        <tr>
          <td>Track</td>
          <td><?=$participant->Track ?></td>
        </tr>
        <tr>
          <td>Gender</td>
          <td><?php $gender = ($participant->Gender)?'Male':'Female'; echo $gender; ?></td>
        </tr>
        <tr>
          <td>Semester</td>
          <td><?=$participant->Semester ?></td>
        </tr>
        <tr>
          <td>College</td>
          <td><?=$participant->College ?></td>
        </tr>
        <tr>
          <td>Department</td>
          <td><?=$participant->Department ?></td>
        </tr>
        <tr>
          <td>City</td>
          <td><?=$participant->City ?></td>
        </tr>
        <tr>
          <td>Previous Camp</td>
          <td><?=$participant->PreviousCamp ?></td>
        </tr>
        <tr>
          <td>Laptop</td>
          <td><?=$participant->Laptop ?></td>
        </tr>
        <tr>
          <td>Tee</td>
          <td><?=$participant->Tee ?></td>
        </tr>
        <tr>
          <td>Date</td>
          <td><?=$participant->Date ?></td>
        </tr>
      </tbody>
    </table>

  </div>
</body>
<?php
  require '../includes/foot.php';
}
else {
  header("Location: $baseurl/view/signin.php");
}
?>

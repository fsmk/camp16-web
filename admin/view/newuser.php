<?php
  session_start();
  require '../includes/config.php';
  require '../includes/head.php';
  if ($_SESSION['signin_check'] == 1) {
?>

<body>
  <?php
    require '../includes/nav.php';
  ?>
  <div class="container">

    <h2>New User</h2>
    <form name="newuser" action="../action/newuser.php" class="form" method="post">
      <div class="form-group">
        <input class="form-control" type="text" name="name" id="name" placeholder="Name" autofocus required>
      </div>
      <div class="form-group">
        <input class="form-control" type="text" name="username" id="username" placeholder="Username">
      </div>
      <div class="form-group">
        <input type="submit" value="Add User" class="btn btn-success">
      </div>
    </form>

  </div>
</body>
<script type="text/javascript">
var t1 = document.getElementById('name');
var t2 = document.getElementById('username');

t1.addEventListener('keyup', function () { t2.value = t1.value.replace(/[^\w]+/g, '-'); }, true);

</script>
<?php
  require '../includes/foot.php';
}
else {
  header("Location: $baseurl/view/signin.php");
}
?>

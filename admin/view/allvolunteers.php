<?php
  session_start();
  require '../includes/config.php';
  require '../includes/head.php';
  if ($_SESSION['signin_check'] == 1) {
    $sql = "SELECT * FROM volunteers";
    $volunteers = mysqli_query($conn, $sql);
?>

<body>
  <?php
    require '../includes/nav.php';
  ?>
  <div class="container">

    <h2>All Volunteers</h2>
    <a href="<?=$baseurl ?>/view/newvolunteer.php" class="btn btn-success">Add Volunteer</a>
    <table class="table">
      <thead>
        <th>ID</th>
        <th>Name</th>
        <th>Email</th>
        <th>Phone</th>
        <th>Track</th>
        <th>College</th>
        <th>Picture</th>
      </thead>
      <tbody>
        <?php
          while ($volunteer = mysqli_fetch_object($volunteers)) {
        ?>
        <tr>
          <td><?=$volunteer->ID ?></td>
          <td><?=$volunteer->Name ?></td>
          <td><?=$volunteer->Email ?></td>
          <td><?=$volunteer->Phone ?></td>
          <td><?=$volunteer->Track ?></td>
          <td><?=$volunteer->College ?></td>
          <td><?=$volunteer->Picture ?></td>
        </tr>
        <?php
      }
        ?>
    </table>

  </div>
</body>
<?php
  require '../includes/foot.php';
}
else {
  header("Location: $baseurl/view/signin.php");
}
?>
